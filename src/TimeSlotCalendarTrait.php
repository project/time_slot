<?php

namespace Drupal\time_slot;

use Drupal\Core\Datetime\DateHelper;
use Drupal\time_slot\Entity\TimeSlot;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList;

/**
 * Class TimeSlotCalendarTrait.
 */
trait TimeSlotCalendarTrait {

  use StringTranslationTrait;

  /**
   * Week days.
   *
   * @return array
   *   Week days.
   */
  public function getWeekDays() {
    $week_days = DateHelper::weekDays(TRUE);

    // Move sunday to the end. Preserve keys with monday = 1.
    $sunday = $week_days[0];
    unset($week_days[0]);
    $week_days[] = $sunday;

    return $week_days;
  }

  /**
   * Transform time of the day in string to minutes since midnight.
   *
   * @param string $time_of_the_day
   *   Time of the day string.
   *
   * @return int
   *   Minutes since midnight.
   */
  public function timeOfTheDayToMinutesSinceMidnight($time_of_the_day = '') {
    return floor(strtotime($time_of_the_day . ' UTC', 0) / 60);
  }

  /**
   * Return a render ready week table filled with content.
   *
   * @param array $content
   *   Array of single content pieces.
   *    'day' - integer - 1 is fist day etc.
   *    'offset' - integer - Minutes since midnight, should be mod 15
   *    'length' - integer - Length in minutes, should be mod 15.
   * @param int $year
   *   Year to render or null for generic.
   * @param int $week
   *   Week number to render or null for generic.
   *
   * @return array
   *   Render array.
   */
  public function weeklyCalendar(array $content, $year = NULL, $week = NULL) {

    /** @var \Drupal\time_slot\TimeSlotViewBuilder $timeSlotViewBuilder */
    $timeSlotViewBuilder = \Drupal::entityTypeManager()->getViewBuilder('time_slot');

    $week_days = $this->getWeekDays();

    $table_header = [];
    $start_date = new \DateTime('midnight');
    foreach ($week_days as $key => $day) {
      if ($year && $week) {
        $start_date->setISODate($year, $week, (int) $key);
        $table_header[] = $day . ' ' . $start_date->format('d.m.');
      }
      else {
        $table_header[] = $day;
      }
    }
    array_unshift($table_header, $this->t('Hour'));

    $table = [
      '#type' => 'table',
      '#header' => $table_header,
      '#attributes' => [
        'class' => [
          'time-slot-weekly-table',
        ],
      ],
      '#attached' => [
        'library' => [
          'time_slot/weekly_table_styling',
        ],
      ],
    ];

    for ($i = 0; $i <= 95; $i++) {

      foreach ($table_header as $key => $header_content) {
        $table[$i][$key] = [];
      }

      // Prepare empty row.
      $table[$i]['#attributes'] = [
        'class' => [
          'time-slot-row-empty',
        ],
      ];
    }

    for ($i = 0; $i <= 95; $i++) {

      if ($i % 4 == 0) {
        $table[$i][0] = [
          '#type' => 'html_tag',
          '#tag' => 'b',
          '#value' => floor($i / 4),
          '#wrapper_attributes' => [
            'class' => [
              'time-column',
              'hour',
            ],
          ],
        ];
      }
      else {
        $table[$i][0] = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $i % 4 * 15,
          '#wrapper_attributes' => [
            'class' => [
              'time-column',
              'minutes-of-hour',
            ],
          ],
        ];
      }

      // Add in content.
      foreach ($content as $index => $time_slot) {
        if (($time_slot['offset'] - ($i * 15)) < 15) {

          $rowspan = (int) ($time_slot['length'] / 15);

          for ($content_row_offset = 0; $content_row_offset < $rowspan; $content_row_offset++) {
            $table[$i + $content_row_offset]['#attributes'] = [
              'class' => [
                'time-slot-content-row',
              ],
            ];
          }

          $table[$i][$time_slot['day']] = [
            'time_slot' => [
              '#type' => 'container',
              'content' => $time_slot['content'],
              '#attributes' => [
                'class' => [
                  'time-slot',
                  $timeSlotViewBuilder->getStateClass(TimeSlot::STATUS_ENABLED),
                ],
                'data-day' => $time_slot['day'],
                'data-offset' => $time_slot['offset'],
                'data-length' => $time_slot['length'],
              ],
            ],
            '#wrapper_attributes' => ['rowspan' => $rowspan],
          ];
          unset($content[$index]);
        }
      }
    }

    return $table;
  }

  /**
   * Take JSON return elements for weekly calendar table.
   *
   * @param string $json
   *   JSON encoded calendar content.
   *
   * @return array
   *   Calendar elements.
   */
  public function weeklyContentByJson($json = '') {
    $slot_elements = [];
    foreach (\json_decode($json, TRUE) as $entry) {

      $entry['day'] = (int) $entry['day'];
      $entry['offset'] = (int) $entry['offset'];
      $entry['length'] = (int) $entry['length'];
      $entry['content'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => gmdate("H:i", $entry['offset'] * 60) . ' - ' . gmdate('H:i', $entry['offset'] * 60 + $entry['length'] * 60),
      ];
      $slot_elements[] = $entry;
    }

    return $slot_elements;
  }

  /**
   * Entity and field_name return element for weekly calendar table.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param string $field_name
   *   Field name.
   *
   * @return array
   *   Calendar element.
   */
  public function weeklyContentItemByEntityField(ContentEntityInterface $entity, $field_name) {
    if (!$entity->hasField($field_name)) {
      return [];
    }

    $field = $entity->get($field_name);

    if (!($field instanceof DateRangeFieldItemList)) {
      return [];
    }

    /** @var \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem $date_item */
    $date_item = $field->get(0);

    if ($date_item->isEmpty()) {
      return [];
    }

    $utc_timezone = new \DateTimeZone(DATETIME_STORAGE_TIMEZONE);
    $user_timezone = new \DateTimeZone(drupal_get_user_timezone());

    $start = new \DateTime($date_item->get('value')->getValue(), $utc_timezone);
    $start->setTimezone($user_timezone);
    $start_midnight = clone $start;
    $start_midnight->modify('midnight');
    $end = new \DateTime($date_item->get('end_value')->getValue(), $utc_timezone);
    $end->setTimezone($user_timezone);
    return [
      'day' => (int) $start->format('N'),
      // TODO: Timezone interferes here.
      'offset' => floor(($start->getTimestamp() - $start_midnight->getTimestamp()) % 86400 / 60),
      'length' => floor(($end->getTimestamp() - $start->getTimestamp()) / 60),
      'content' => \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId())->view($entity),
    ];
  }

  /**
   * Take JSON return elements for weekly calendar table.
   *
   * @param \Drupal\time_slot\Entity\TimeSlotInterface[] $time_slots
   *   JSON encoded calendar content.
   *
   * @return array
   *   Calendar elements.
   */
  public function weeklyContentByTimeSlots(array $time_slots) {
    $time_slot_elements = [];
    foreach ($time_slots as $id => $time_slot) {

      $utc_timezone = new \DateTimeZone(DATETIME_STORAGE_TIMEZONE);
      $user_timezone = new \DateTimeZone(drupal_get_user_timezone());

      $start = new \DateTime($time_slot->getDateRange()->get('value')->getValue(), $utc_timezone);
      $start->setTimezone($user_timezone);
      $start_midnight = clone $start;
      $start_midnight->modify('midnight');
      $end = new \DateTime($time_slot->getDateRange()->get('end_value')->getValue(), $utc_timezone);
      $end->setTimezone($user_timezone);
      $time_slot_elements[] = [
        'day' => (int) $start->format('N'),
        // TODO: Timezone interferes here.
        'offset' => floor(($start->getTimestamp() - $start_midnight->getTimestamp()) % 86400 / 60),
        'length' => floor(($end->getTimestamp() - $start->getTimestamp()) / 60),
        'content' => \Drupal::entityTypeManager()->getViewBuilder('time_slot')->view($time_slot, 'calendar'),
      ];
    }

    return $time_slot_elements;
  }

}
