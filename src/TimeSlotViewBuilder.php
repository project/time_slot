<?php

namespace Drupal\time_slot;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\time_slot\Entity\TimeSlot;

/**
 * View builder handler for time slots.
 */
class TimeSlotViewBuilder extends EntityViewBuilder {

  /**
   * Status CSS class mapping.
   *
   * @var array
   */
  protected $statusClasses = [
    TimeSlot::STATUS_DISABLED => 'time-slot-disabled',
    TimeSlot::STATUS_ENABLED => 'time-slot-enabled',
    TimeSlot::STATUS_OCCUPIED => 'time-slot-occupied',
    TimeSlot::STATUS_BLOCKED => 'time-slot-blocked',
  ];

  /**
   * TimeSlot State class mapping.
   *
   * @param string $state
   *   State.
   *
   * @return string
   *   CSS class.
   */
  public function getStateClass($state) {
    if (isset($this->statusClasses[$state])) {
      return $this->statusClasses[$state];
    }

    return $this->statusClasses[TimeSlot::STATUS_DISABLED];
  }

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    /** @var \Drupal\time_slot\Entity\TimeSlotInterface $entity */
    parent::alterBuild($build, $entity, $display, $view_mode);
    if ($entity->id()) {
      $state = $entity->getState()->getValue()['value'];
      $build['#state_class'] = $this->getStateClass($state);
      $build['#contextual_links']['time_slot'] = [
        'route_parameters' => [
          'time_slot' => $entity->id(),
        ],
      ];
    }
  }

}
