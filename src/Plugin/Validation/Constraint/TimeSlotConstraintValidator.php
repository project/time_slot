<?php

namespace Drupal\time_slot\Plugin\Validation\Constraint;

use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\time_slot\TimeSlotStorageInterface;

/**
 * TimeSlot constraint validator.
 */
class TimeSlotConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time slot storage.
   *
   * @var \Drupal\time_slot\TimeSlotStorageInterface
   */
  protected $timeSlotStorage;

  /**
   * Constructs a new CommentNameConstraintValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\time_slot\TimeSlotStorageInterface $time_slot_storage
   *   The time slot storage.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TimeSlotStorageInterface $time_slot_storage) {
    $this->entityTypeManager = $entity_type_manager;
    $this->timeSlotStorage = $time_slot_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('time_slot')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($time_slot, Constraint $constraint) {
    /** @var \Drupal\time_slot\Plugin\Validation\Constraint\TimeSlotConstraint $constraint */
    if (isset($time_slot)) {
      /** @var \Drupal\time_slot\Entity\TimeSlotInterface $time_slot */
      $source_type = $time_slot->get('source_content_type')->getValue();
      if (empty($source_type)) {
        $this->context->addViolation($constraint->messageSourceEntityTypeMissing);
      }

      $source_id = $time_slot->get('source_content_id')->getValue();
      if (empty($source_id)) {
        $this->context->addViolation($constraint->messageSourceEntityIdMissing);
      }

      $source_field = $time_slot->get('source_content_field')->getValue();
      if (empty($source_field)) {
        $this->context->addViolation($constraint->messageSourceEntityFieldMissing);
      }

      if (
        empty($source_type)
        || empty($source_id)
        || empty($source_field)
      ) {
        return;
      }

      $source_type = $source_type[0]['value'];
      $source_id = $source_id[0]['value'];
      $source_field = $source_field[0]['value'];

      try {
        $source_entity_storage = $this->entityTypeManager->getStorage($source_type);
      }
      catch (InvalidPluginDefinitionException $e) {
        $this->context->addViolation($constraint->messageSourceEntityTypeInvalid);
        return;
      }

      $source_entity = $source_entity_storage->load($source_id);

      if (empty($source_entity)) {
        $this->context->addViolation($constraint->messageSourceEntityMissing);
        return;
      }

      if (!($source_entity instanceof ContentEntityInterface)) {
        $this->context->addViolation($constraint->messageSourceEntityNotFieldable);
        return;
      }

      if (!$source_entity->hasField($source_field)) {
        $this->context->addViolation($constraint->messageSourceEntityFieldNotExisting);
      }

      if ($source_entity->getFieldDefinition($source_field)->getType() !== 'time_slot_management') {
        $this->context->addViolation($constraint->messageSourceEntityFieldWrongType);
      }

      $date_range = $time_slot->getDateRange();

      if (empty($date_range)) {
        // Will trigger constraint by being required in core.
        return;
      }

      $query = \Drupal::entityQuery('time_slot');
      $query->condition('source_content_type', $source_type);
      $query->condition('source_content_id', $source_id);
      $query->condition('source_content_field', $source_field);
      $query->condition('date.value', $date_range->getValue()['end_value'], '<');
      $query->condition('date.end_value', $date_range->getValue()['value'], '>');

      $overlapping_time_slots = $query->execute();

      if (!empty($overlapping_time_slots)) {
        $this->context->addViolation($constraint->messageOverlapNotPossible);
      }
    }
  }

}
