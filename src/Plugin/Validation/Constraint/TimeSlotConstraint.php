<?php

namespace Drupal\time_slot\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Time slot constraint.
 *
 * @Constraint(
 *   id = "TimeSlot",
 *   label = @Translation("Time slot", context = "Validation"),
 *   type = "entity:time_slot"
 * )
 */
class TimeSlotConstraint extends Constraint {

  public $messageSourceEntityTypeMissing = 'Source entity type missing.';
  public $messageSourceEntityTypeInvalid = 'Source entity type invalid.';
  public $messageSourceEntityIdMissing = 'Source entity ID missing.';
  public $messageSourceEntityFieldMissing = 'Source entity field missing.';
  public $messageSourceEntityMissing = 'Source entity missing.';
  public $messageSourceEntityNotFieldable = 'Source entity not fieldable.';
  public $messageSourceEntityFieldNotExisting = 'Source entity field does not exist.';
  public $messageSourceEntityFieldWrongType = 'Source entity field is of wrong type.';
  public $messageOverlapNotPossible = 'Overlapping time slot found. Overlapping time slots cannot exist.';

}
