<?php

namespace Drupal\time_slot\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\time_slot\TimeSlotCalendarTrait;
use Drupal\time_slot\Entity\TimeSlot;

/**
 * Plugin implementation of the time_slot_management_pattern_widget_type widget.
 *
 * @FieldWidget(
 *   id = "time_slot_management_weekly_pattern",
 *   label = @Translation("Time slot management pattern"),
 *   field_types = {
 *     "time_slot_management"
 *   }
 * )
 */
class TimeSlotManagementWeeklyPattern extends WidgetBase {

  use TimeSlotCalendarTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $storage_definition = $this->fieldDefinition->getFieldStorageDefinition();

    $element['time_slot_management_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Time Slot Management'),
      '#open' => TRUE,
      '#attributes' => [
        'class' => [
          'time-slot-management-weekly-pattern-container',
        ],
      ],
      '#attached' => [
        'library' => [
          'time_slot/management_widget',
        ],
      ],
    ];

    $time_slot_types = [];
    foreach (\Drupal::entityTypeManager()->getStorage('time_slot_type')->loadMultiple() as $time_slot_type_id => $time_slot_type) {
      $time_slot_types[$time_slot_type_id] = $time_slot_type->label();
    }

    $element['time_slot_management_container']['time_slot_type'] = [
      '#title' => $this->t('Time slot type'),
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->time_slot_type) ? $items[$delta]->time_slot_type : $storage_definition->getSetting('time_slot_type'),
      '#options' => $time_slot_types,
    ];

    $default_slot_length_value = isset($items[$delta]->default_slot_length) ? $items[$delta]->default_slot_length : $storage_definition->getSetting('default_slot_length');
    $element['time_slot_management_container']['default_slot_length'] = [
      '#title' => $this->t('Default slot length'),
      '#description' => $this->t('Slot length in minutes'),
      '#type' => 'number',
      '#min' => 15,
      '#max' => 120,
      '#step' => 15,
      '#default_value' => $default_slot_length_value,
    ];

    $default_daily_slots_value = isset($items[$delta]->default_daily_slots) ? $items[$delta]->default_daily_slots : (int) $storage_definition->getSetting('default_daily_slots');
    $element['time_slot_management_container']['default_daily_slots'] = [
      '#title' => $this->t('Default number of slots'),
      '#description' => $this->t('The offset + slot length * number cannot exceed 24h or 1440min'),
      '#type' => 'number',
      '#default_value' => $default_daily_slots_value,
      '#min' => 1,
      // 24h * 15min = 96.
      '#max' => 96,
      '#step' => 1,
    ];

    $default_daily_offset_value = isset($items[$delta]->default_daily_offset) ? $items[$delta]->default_daily_offset : (int) $storage_definition->getSetting('default_daily_offset');
    $element['time_slot_management_container']['default_daily_offset'] = [
      '#title' => $this->t('Default offset'),
      '#description' => $this->t('Minutes since midnight to begin of first slot'),
      '#type' => 'textfield',
      '#default_value' => gmdate('H:i', $default_daily_offset_value * 60),
      '#min' => 0,
      '#max' => 1440,
      '#step' => 1,
    ];

    $default_week_days_value = [];
    foreach (array_keys($this->getWeekDays()) as $key) {
      $default_week_days_value[$key] = $key;
    }
    $default_week_days_value = isset($items[$delta]->default_week_days) ? \json_decode($items[$delta]->default_week_days, TRUE) : $default_week_days_value;
    $element['time_slot_management_container']['default_week_days'] = [
      '#title' => $this->t('Default week days'),
      '#type' => 'checkboxes',
      '#default_value' => $default_week_days_value,
      '#options' => $this->getWeekDays(),
    ];

    $element['time_slot_management_container']['weekly_pattern'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'time-slot-weekly-pattern-container',
        ],
      ],
    ];

    $default_weekly_pattern = !empty($items[$delta]->weekly_pattern) ? $items[$delta]->weekly_pattern : $this->weeklyJsonByPattern(
      $default_slot_length_value,
      $default_daily_offset_value,
      $default_daily_slots_value,
      $default_week_days_value
    );

    $element['time_slot_management_container']['weekly_pattern']['weekly_pattern_input'] = [
      '#type' => 'hidden',
      '#default_value' => $default_weekly_pattern,
      '#attributes' => ['class' => ['time-slot-hidden-weekly-pattern']],
    ];

    $element['time_slot_management_container']['weekly_pattern']['weekly_pattern_table_add'] = [
      '#type' => 'button',
      '#value' => $this->t('Add to weekly pattern table'),
      '#attributes' => [
        'class' => [
          'time-slot-management-weekly-pattern-add',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'addToWeeklyPatternTable'],
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $element['time_slot_management_container']['weekly_pattern']['weekly_pattern_table_reset'] = [
      '#type' => 'button',
      '#value' => $this->t('Reset weekly pattern table'),
      '#attributes' => [
        'class' => [
          'time-slot-management-weekly-pattern-reset',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'resetWeeklyPatternTable'],
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    if (!empty($default_weekly_pattern)) {
      $slot_elements = $this->weeklyContentByJson($default_weekly_pattern);
    }
    else {
      $slot_elements = $this->weeklyContentByPattern(
        $default_slot_length_value,
        $default_daily_offset_value,
        $default_daily_slots_value,
        $default_week_days_value
      );
    }

    $element['time_slot_management_container']['weekly_pattern']['weekly_pattern_table'] = $this->weeklyCalendar($slot_elements);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $return_values = [];
    $values = parent::massageFormValues($values, $form, $form_state);

    foreach ($values as $value) {
      $value['time_slot_management_container']['default_daily_offset'] = $this->timeOfTheDayToMinutesSinceMidnight($value['time_slot_management_container']['default_daily_offset']);
      $value['time_slot_management_container']['default_week_days'] = json_encode($value['time_slot_management_container']['default_week_days']);
      $value['time_slot_management_container']['weekly_pattern'] = $value['time_slot_management_container']['weekly_pattern']['weekly_pattern_input'];
      $return_values[$value['_original_delta']] = $value['time_slot_management_container'];
    }

    return $return_values;
  }

  /**
   * Ajax response handler.
   *
   * @param array $form
   *   Current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return string
   *   Updated weekly pattern table HTML.
   */
  public function resetWeeklyPatternTable(array $form, FormStateInterface $form_state) {

    $triggering_element = $form_state->getTriggeringElement();
    $triggering_array_parents = FALSE;
    $triggering_parents = FALSE;
    if (!empty($triggering_element['#parents'])) {
      array_pop($triggering_element['#array_parents']);
      array_pop($triggering_element['#array_parents']);
      array_pop($triggering_element['#parents']);
      array_pop($triggering_element['#parents']);
      $triggering_array_parents = $triggering_element['#array_parents'];
      $triggering_parents = $triggering_element['#parents'];
    }

    $time_slot_container = NestedArray::getValue($form, $triggering_array_parents);

    $values = $form_state->getValue($triggering_parents, FALSE);

    $slot_elements = $this->weeklyContentByPattern(
      $values['default_slot_length'],
      $this->timeOfTheDayToMinutesSinceMidnight($values['default_daily_offset']),
      $values['default_daily_slots'],
      $values['default_week_days']
    );

    $json_content = $this->weeklyJsonByPattern(
      $values['default_slot_length'],
      $this->timeOfTheDayToMinutesSinceMidnight($values['default_daily_offset']),
      $values['default_daily_slots'],
      $values['default_week_days']
    );

    $table = $this->weeklyCalendar($slot_elements);

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('[data-drupal-selector=' . $time_slot_container['#attributes']['data-drupal-selector'] . '] .time-slot-weekly-table', $table));
    $response->addCommand(new InvokeCommand('[data-drupal-selector=' . $time_slot_container['#attributes']['data-drupal-selector'] . '] .time-slot-hidden-weekly-pattern', 'val', [$json_content]));

    return $response;
  }

  /**
   * Ajax response handler.
   *
   * @param array $form
   *   Current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return string
   *   Updated weekly pattern table HTML.
   */
  public function addToWeeklyPatternTable(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $triggering_array_parents = FALSE;
    $triggering_parents = FALSE;
    if (!empty($triggering_element['#parents'])) {
      array_pop($triggering_element['#array_parents']);
      array_pop($triggering_element['#array_parents']);
      array_pop($triggering_element['#parents']);
      array_pop($triggering_element['#parents']);
      $triggering_array_parents = $triggering_element['#array_parents'];
      $triggering_parents = $triggering_element['#parents'];
    }

    $time_slot_container = NestedArray::getValue($form, $triggering_array_parents);

    $values = $form_state->getValue($triggering_parents, FALSE);

    $existing_slot_elements = $this->weeklyContentByJson($values['weekly_pattern']['weekly_pattern_input']);

    $new_slot_elements = $this->weeklyContentByPattern(
      $values['default_slot_length'],
      $this->timeOfTheDayToMinutesSinceMidnight($values['default_daily_offset']),
      $values['default_daily_slots'],
      $values['default_week_days']
    );

    $slot_elements = array_merge_recursive($new_slot_elements, $existing_slot_elements);

    $json_content = $this->weeklyJsonByElements($slot_elements);

    $table = $this->weeklyCalendar($slot_elements);

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('[data-drupal-selector=' . $time_slot_container['#attributes']['data-drupal-selector'] . '] .time-slot-weekly-table', $table));
    $response->addCommand(new InvokeCommand('[data-drupal-selector=' . $time_slot_container['#attributes']['data-drupal-selector'] . '] .time-slot-hidden-weekly-pattern', 'val', [$json_content]));

    return $response;
  }

  /**
   * Return JSON for time slot weekly calendar.
   *
   * @param int $slot_length
   *   Slot length in minutes.
   * @param int $daily_offset
   *   Daily offset in minutes from midnight.
   * @param int $daily_slots
   *   Number of slots per day.
   * @param array $days
   *   Enabled days by index, starting 1 for monday.
   *
   * @return string
   *   JSON encoded patter.
   */
  public function weeklyJsonByPattern($slot_length, $daily_offset, $daily_slots, array $days) {

    $content = [];

    $slot_length = max((int) $slot_length, 15);
    $daily_offset = (int) $daily_offset ?: 0;
    $daily_slots = (int) $daily_slots ?: 0;

    foreach ($days as $i => $state) {
      if (!$state) {
        continue;
      }
      for ($j = 0; $j < $daily_slots; $j++) {
        $offset = $daily_offset + $j * $slot_length;

        $content[] = [
          'day' => $i,
          'offset' => $offset,
          'length' => $slot_length,
          'status' => TimeSlot::STATUS_ENABLED,
        ];
      }
    }

    return \json_encode($content);
  }

  /**
   * Return content for time slot weekly calendar.
   *
   * @param int $slot_length
   *   Slot length in minutes.
   * @param int $daily_offset
   *   Daily offset in minutes from midnight.
   * @param int $daily_slots
   *   Number of slots per day.
   * @param array $days
   *   Enabled days by index, starting 1 for monday.
   *
   * @return array
   *   Content elements to feed to calendar.
   */
  public function weeklyContentByPattern($slot_length, $daily_offset, $daily_slots, array $days) {

    $content = [];

    $slot_length = max((int) $slot_length, 15);
    $daily_offset = (int) $daily_offset ?: 0;
    $daily_slots = (int) $daily_slots ?: 0;

    foreach ($days as $i => $state) {
      if (!$state) {
        continue;
      }
      for ($j = 0; $j < $daily_slots; $j++) {
        $offset = $daily_offset + $j * $slot_length;

        $content[] = [
          'day' => $i,
          'offset' => $offset,
          'length' => $slot_length,
          'status' => TimeSlot::STATUS_ENABLED,
          'content' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => gmdate("H:i", $offset * 60) . ' - ' . gmdate('H:i', $offset * 60 + $slot_length * 60),
          ],
        ];
      }
    }

    return $content;
  }

  /**
   * Return JSON string by calendar elements.
   *
   * @param array $elements
   *   Calendar elements.
   *
   * @return string
   *   JSON encoded string.
   */
  public function weeklyJsonByElements(array $elements) {
    foreach ($elements as $element) {
      unset($element['content']);
    }
    return \json_encode($elements);
  }

}
