<?php

namespace Drupal\time_slot\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'time_slots_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "time_slot_management_link",
 *   label = @Translation("Time slot Management Link"),
 *   field_types = {
 *     "time_slot_management"
 *   }
 * )
 */
class TimeSlotManagementLink extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $time_slot_management_item = $items->get(0);

    /** @var \Drupal\Core\Entity\EntityInterface $parent_entity */
    $parent_entity = $items->getParent()->getValue();

    if (empty($time_slot_management_item)) {
      return [];
    }

    $field_content = [
      'management_link' => [
        '#type' => 'link',
        '#url' => Url::fromRoute('time_slot.management_form', [
          'content_type' => $this->fieldDefinition->getTargetEntityTypeId(),
          'content_id' => $parent_entity->id(),
          'time_slot_management_field_name' => $this->fieldDefinition->getName(),
        ]),
        '#title' => $this->t('Manage time slots'),
      ],
    ];

    return [
      0 => $field_content,
    ];
  }

}
