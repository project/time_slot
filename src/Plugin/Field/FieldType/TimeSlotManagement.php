<?php

namespace Drupal\time_slot\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'time_slot_management_field_type' field type.
 *
 * @FieldType(
 *   id = "time_slot_management",
 *   label = @Translation("Time slot management"),
 *   description = @Translation("Create and manage time slots based on patterns"),
 *   default_widget = "time_slot_management_weekly_pattern",
 *   default_formatter = "time_slot_management_link"
 * )
 *
 * @see https://www.drupal.org/node/2403703
 */
class TimeSlotManagement extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'time_slot_type' => 'default',
      'default_slot_length' => 120,
      'default_daily_offset' => 480,
      'default_daily_slots' => 4,
      'weekly_pattern' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['time_slot_type'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Time slot type'))
      ->setRequired(TRUE);

    $properties['default_slot_length'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Default slot length'))
      ->setDescription(new TranslatableMarkup('Slot length in minutes'))
      ->addConstraint('Range', ['max' => 1440, 'min' => 0])
      ->setRequired(TRUE);

    $properties['default_daily_offset'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Default offset'))
      ->setDescription(new TranslatableMarkup('Minutes since midnight to begin of first slot'))
      ->addConstraint('Range', ['max' => 1440, 'min' => 0])
      ->setRequired(TRUE);

    $properties['default_daily_slots'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Default number of slots'))
      ->setDescription(new TranslatableMarkup('The offset + slot length * number cannot exceed 24h or 1440min'))
      ->setRequired(TRUE);

    $properties['default_week_days'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Default week days'))
      ->setRequired(TRUE);

    $properties['weekly_pattern'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Weekly pattern'))
      ->setDescription(new TranslatableMarkup('JSON encoded array of days, each containing the slots and offsets as above'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'time_slot_type' => [
          'description' => 'Time slot type.',
          'type' => 'varchar',
          'length' => 512,
        ],
        'default_slot_length' => [
          'description' => 'Stores the default slot length.',
          'type' => 'int',
          'not null' => TRUE,
        ],
        'default_daily_slots' => [
          'description' => 'Stores the default number of slots.',
          'type' => 'int',
          'not null' => TRUE,
        ],
        'default_daily_offset' => [
          'description' => 'Stores the default daily offset.',
          'type' => 'int',
          'not null' => TRUE,
        ],
        'default_week_days' => [
          'description' => 'Stores the default week days.',
          'type' => 'blob',
          'not null' => TRUE,
          'serialize' => TRUE,
        ],
        'weekly_pattern' => [
          'description' => 'Serialized array of weekly pattern.',
          'type' => 'blob',
          'size' => 'big',
          'not null' => TRUE,
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'weekly_pattern';
  }

}
