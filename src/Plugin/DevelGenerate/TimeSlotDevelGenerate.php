<?php

namespace Drupal\time_slot\Plugin\DevelGenerate;

use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\time_slot\TimeSlotStorageInterface;
use Drupal\time_slot\Entity\TimeSlot;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\devel_generate\DevelGenerateBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a TimeSlotDevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "time_slot",
 *   label = @Translation("time slots"),
 *   description = @Translation("Generate a given number of time slots. Optionally delete current time slots."),
 *   url = "time-slot",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "num" = 10,
 *     "title_length" = 12,
 *     "kill" = FALSE,
 *   }
 * )
 */
class TimeSlotDevelGenerate extends DevelGenerateBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time slot storage.
   *
   * @var \Drupal\time_slot\TimeSlotStorageInterface
   */
  protected $timeSlotStorage;

  /**
   * Constructs a new TimeSlotDevelGenerate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\time_slot\TimeSlotStorageInterface $time_slot_storage
   *   The time slot storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, TimeSlotStorageInterface $time_slot_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->timeSlotStorage = $time_slot_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('time_slot')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['num'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of terms?'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
      '#min' => 0,
    ];
    $form['kill'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete existing terms in specified vocabularies before generating new time slots.'),
      '#default_value' => $this->getSetting('kill'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function generateElements(array $values) {
    if ($values['kill']) {
      $time_slots = $this->timeSlotStorage->loadMultiple();
      $this->timeSlotStorage->delete($time_slots);
      $this->setMessage($this->t('Deleted existing time slots.'));
    }

    /** @var \Drupal\field\Entity\\FieldStorageConfig[] $time_slot_fields */
    $time_slot_fields = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties([
      'type' => 'time_slot_management',
      'deleted' => FALSE,
      'status' => 1,
    ]);

    $new_time_slots = [];
    for ($i = 0; $i < (int) $values['num']; $i++) {
      /** @var \Drupal\field\Entity\FieldStorageConfig $current_field */
      $current_field = $time_slot_fields[array_rand($time_slot_fields)];
      $query = \Drupal::entityQuery($current_field->getTargetEntityTypeId());
      $query->exists($current_field->getName());
      $query->addTag('random');
      $query->range(0, 1);
      $result = $query->execute();

      $states = [
        TimeSlot::STATUS_DISABLED,
        TimeSlot::STATUS_DISABLED,
        TimeSlot::STATUS_OCCUPIED,
        TimeSlot::STATUS_BLOCKED,
      ];

      /** @var \Drupal\time_slot\Entity\TimeSlotInterface $new_time_slot */
      $new_time_slot = $this->timeSlotStorage->create([
        'type' => 'default',
        'source_content_type' => $current_field->getTargetEntityTypeId(),
        'source_content_id' => array_pop($result),
        'source_content_field' => $current_field->getName(),
        'state' => (int) $states[array_rand($states)],
      ]);

      $date_range = $new_time_slot->get('date');
      $sample_value = DateRangeItem::generateSampleValue($date_range->getFieldDefinition());

      // Floor timestamp to 15min slot.
      $begin = strtotime($sample_value['value']);
      $begin = $begin - ($begin % (15 * 60));
      $sample_value['value'] = date(DATETIME_DATETIME_STORAGE_FORMAT, $begin);

      // Add random multiple of 15-75min for end date.
      $end = rand(1, 5) * 15 * 60;
      $sample_value['end_value'] = date(DATETIME_DATETIME_STORAGE_FORMAT, $begin + $end);

      $new_time_slot->setDateRange($sample_value);

      $new_time_slot->save();
      $new_time_slots[] = $new_time_slot->id();
    }
    if (!empty($new_time_slots)) {
      $this->setMessage($this->t('Created the following new time slots: @time_slots', ['@time_slots' => implode(', ', $new_time_slots)]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateDrushParams($args) {
    $number = array_shift($args);

    if ($number === NULL) {
      $number = 10;
    }

    if (!$this->isNumber($number)) {
      return drush_set_error('DEVEL_GENERATE_INVALID_INPUT', dt('Invalid number of time slots: @num', ['@num' => $number]));
    }

    $values = [
      'num' => $number,
      'kill' => drush_get_option('kill'),
    ];

    return $values;
  }

}
