<?php

namespace Drupal\time_slot\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\time_slot\TimeSlotCalendarTrait;
use Drupal\Core\Url;

/**
 * Display time slots in a weekly calendar.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "time_slot_calendar",
 *   title = @Translation("Time Slot Calendar"),
 *   help = @Translation("Display and en-/disable time slots per week."),
 *   theme = "views_view_list",
 *   display_types = {"normal"},
 * )
 */
class TimeSlotCalendar extends StylePluginBase {

  use TimeSlotCalendarTrait;

  protected $usesFields = TRUE;
  protected $usesRowPlugin = TRUE;
  protected $usesRowClass = FALSE;
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  public function evenEmpty() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {

    if (
      empty($this->options['year_argument'])
      || empty($this->options['year_argument'])
    ) {
      return [];
    }

    /** @var \Drupal\views\Plugin\views\argument\YearDate $year_argument */
    $year_argument = $this->displayHandler->getHandler('argument', $this->options['year_argument']);
    /** @var \Drupal\views\Plugin\views\argument\WeekDate $week_argument */
    $week_argument = $this->displayHandler->getHandler('argument', $this->options['week_argument']);

    if (!empty($this->options['daterange_field'])) {
      $daterange_field_name = $this->options['daterange_field'];
    }
    else {
      return [];
    }

    $calendar_items = [];

    foreach ($this->view->result as $row_number => $row) {
      if ($this->view->field[$daterange_field_name]) {
        $daterange_field = $this->view->field[$daterange_field_name];
        $entity = $daterange_field->getEntity($row);
        $new_item = $this->weeklyContentItemByEntityField($entity, $daterange_field->definition['field_name']);
        if (empty($new_item)) {
          continue;
        }
        $new_item['content'] = $row_content[] = $this->view->rowPlugin->render($row);
        $calendar_items[] = $new_item;
      }
    }

    $build = [];

    $build['navigation'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'time-slot-calendar-views-display-navigation',
        ],
      ],
      '#attached' => [
        'library' => [
          'time_slot/time_slot_calendar_views_display',
        ],
      ],
    ];

    $build['calendar'] = $this->weeklyCalendar($calendar_items, $year_argument->getValue(), $week_argument->getValue());

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['year_argument'] = ['default' => ''];
    $options['week_argument'] = ['default' => ''];
    $options['daterange_field'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $year_options = [];
    $week_options = [];

    /** @var \Drupal\views\Plugin\views\argument\ArgumentPluginBase[] $arguments */
    $arguments = $this->displayHandler->getHandlers('argument');
    foreach ($arguments as $argument_name => $argument) {
      switch ($argument->getPluginId()) {
        case 'datetime_year':
          $year_options[$argument_name] = $argument->adminLabel();
          break;

        case  'datetime_week':
          $week_options[$argument_name] = $argument->adminLabel();
          break;
      }
    }

    $form['year_argument'] = [
      '#title' => $this->t('Year source argument'),
      '#type' => 'select',
      '#default_value' => $this->options['year_argument'],
      '#description' => $this->t("Year argument for calendar."),
      '#options' => $year_options,
      '#required' => TRUE,
    ];

    $form['week_argument'] = [
      '#title' => $this->t('Week source argument'),
      '#type' => 'select',
      '#default_value' => $this->options['week_argument'],
      '#description' => $this->t("Week argument for calendar."),
      '#options' => $week_options,
      '#required' => TRUE,
    ];

    $labels = $this->displayHandler->getFieldLabels();
    $fieldMap = \Drupal::service('entity_field.manager')->getFieldMap();
    $daterange_options = [];

    $fields = $this->displayHandler->getOption('fields');
    foreach ($fields as $field_name => $field) {
      if ($field['type'] == 'daterange_default') {
        $daterange_options[$field_name] = $labels[$field_name];
      }

      if (
        $field['plugin_id'] == 'field'
        && !empty($field['entity_type'])
        && !empty($field['entity_field'])
      ) {
        if (
          !empty($fieldMap[$field['entity_type']][$field['entity_field']]['type'])
          && $fieldMap[$field['entity_type']][$field['entity_field']]['type'] == 'daterange'
        ) {
          $daterange_options[$field_name] = $labels[$field_name];
        }
      }
    }

    $form['daterange_field'] = [
      '#title' => $this->t('Daterange source field'),
      '#type' => 'select',
      '#default_value' => $this->options['daterange_field'],
      '#description' => $this->t("The source of date range for each entity."),
      '#options' => $daterange_options,
      '#required' => TRUE,
    ];

  }

}
