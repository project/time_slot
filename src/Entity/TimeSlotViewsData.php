<?php

namespace Drupal\time_slot\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Time slot entities.
 */
class TimeSlotViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Set the 'datetime' filter type.
    $data['time_slot']['date__value']['filter']['id'] = 'datetime';
    $data['time_slot']['date__end_value']['filter']['id'] = 'datetime';

    // Set the 'datetime' argument type.
    $data['time_slot']['date__value']['argument']['id'] = 'datetime';
    $data['time_slot']['date__end_value']['argument']['id'] = 'datetime';

    // Create year, month, and day arguments.
    $arguments = [
      // Argument type => help text.
      'year' => t('Date in the form of YYYY.'),
      'month' => t('Date in the form of MM (01 - 12).'),
      'day' => t('Date in the form of DD (01 - 31).'),
      'week' => t('Date in the form of WW (01 - 53).'),
      'year_month' => t('Date in the form of YYYYMM.'),
      'full_date' => t('Date in the form of CCYYMMDD.'),
    ];
    foreach ($arguments as $argument_type => $help_text) {
      $data['time_slot']['date__value_' . $argument_type] = [
        'title' => $data['time_slot']['date__value']['title'] . ' (' . $argument_type . ')',
        'help' => $help_text,
        'argument' => [
          'field' => 'date__value',
          'id' => 'datetime_' . $argument_type,
        ],
      ];

      $data['time_slot']['date__end_value_' . $argument_type] = [
        'title' => $data['time_slot']['date__end_value']['title'] . ' (' . $argument_type . ')',
        'help' => $help_text,
        'argument' => [
          'field' => 'date__end_value',
          'id' => 'datetime_' . $argument_type,
        ],
      ];
    }

    // Set the 'datetime' sort handler.
    $data['time_slot']['date__value']['sort']['id'] = 'datetime';
    $data['time_slot']['date__end_value']['sort']['id'] = 'datetime';

    return $data;
  }

}
