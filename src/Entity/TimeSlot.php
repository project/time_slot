<?php

namespace Drupal\time_slot\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Time slot entity.
 *
 * @ingroup time_slot
 *
 * @ContentEntityType(
 *   id = "time_slot",
 *   label = @Translation("Time slot"),
 *   bundle_label = @Translation("Time slot type"),
 *   handlers = {
 *     "storage" = "Drupal\time_slot\TimeSlotStorage",
 *     "view_builder" = "Drupal\time_slot\TimeSlotViewBuilder",
 *     "list_builder" = "Drupal\time_slot\TimeSlotListBuilder",
 *     "views_data" = "Drupal\time_slot\Entity\TimeSlotViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\time_slot\Form\TimeSlotForm",
 *       "add" = "Drupal\time_slot\Form\TimeSlotForm",
 *       "edit" = "Drupal\time_slot\Form\TimeSlotForm",
 *       "delete" = "Drupal\time_slot\Form\TimeSlotDeleteForm",
 *     },
 *   },
 *   base_table = "time_slot",
 *   admin_permission = "administer time slot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   bundle_entity_type = "time_slot_type",
 *   field_ui_base_route = "entity.time_slot_type.edit_form",
 *   constraints = {
 *     "TimeSlot" = {}
 *   }
 * )
 */
class TimeSlot extends ContentEntityBase implements TimeSlotInterface {

  use EntityChangedTrait;

  const STATUS_DISABLED = 0;
  const STATUS_ENABLED = 1;
  const STATUS_OCCUPIED = 2;
  const STATUS_BLOCKED = 3;

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = $this->getDateRange()->view([
      'label' => 'hidden',
      'type' => 'daterange_custom',
      'settings' => [
        'date_format' => 'H:i',
      ],
    ]);

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDateRange() {
    return $this->get('date')->get(0);
  }

  /**
   * {@inheritdoc}
   */
  public function setDateRange(array $date_range_values) {
    return $this->set('date', $date_range_values);
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->get(0);
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state) {
    return $this->set('state', (int) $state);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['date'] = BaseFieldDefinition::create('daterange')
      ->setLabel(t('Date range'))
      ->setDescription(t('The date range.'))
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayOptions('view', [
        'type' => 'daterange_default',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'daterange_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['source_content_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source entity type'))
      ->setRequired(TRUE);

    $fields['source_content_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Source entity ID'))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE);

    $fields['source_content_field'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source entity field name'))
      ->setRequired(TRUE);

    $fields['state'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('State'))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        self::STATUS_DISABLED => t('Disabled'),
        self::STATUS_ENABLED => t('Enabled'),
        self::STATUS_OCCUPIED => t('Occupied'),
        self::STATUS_BLOCKED => t('Blocked'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
