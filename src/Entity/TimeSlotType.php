<?php

namespace Drupal\time_slot\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\time_slot\Entity\TimeSlotTypeInterface;

/**
 * Defines the Time slot type entity.
 *
 * @ConfigEntityType(
 *   id = "time_slot_type",
 *   label = @Translation("Time slot type"),
 *   handlers = {
 *     "list_builder" = "Drupal\time_slot\TimeSlotTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\time_slot\Form\TimeSlotTypeForm",
 *       "edit" = "Drupal\time_slot\Form\TimeSlotTypeForm",
 *       "delete" = "Drupal\time_slot\Form\TimeSlotTypeDeleteForm"
 *     },
 *   },
 *   config_prefix = "time_slot_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "time_slot",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/time_slot_type/{time_slot_type}",
 *     "add-form" = "/admin/structure/time_slot_type/add",
 *     "edit-form" = "/admin/structure/time_slot_type/{time_slot_type}/edit",
 *     "delete-form" = "/admin/structure/time_slot_type/{time_slot_type}/delete",
 *     "collection" = "/admin/structure/time_slot_types"
 *   }
 * )
 */
class TimeSlotType extends ConfigEntityBundleBase implements TimeSlotTypeInterface {
  /**
   * The Time slot entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Time slot entity type label.
   *
   * @var string
   */
  protected $label;

}
