<?php

namespace Drupal\time_slot\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Time slot entities.
 *
 * @ingroup time_slot
 */
interface TimeSlotInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Time slot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Time slot.
   */
  public function getCreatedTime();

  /**
   * Sets the Time slot creation timestamp.
   *
   * @param int $timestamp
   *   The Time slot creation timestamp.
   *
   * @return \Drupal\time_slot\Entity\TimeSlotInterface
   *   The called Time slot entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Time slot published status indicator.
   *
   * Unpublished Time slot are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Time slot is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Time slot.
   *
   * @param bool $published
   *   TRUE to set this Time slot to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\time_slot\Entity\TimeSlotInterface
   *   The called Time slot entity.
   */
  public function setPublished($published);

  /**
   * Return time slot date range.
   *
   * @return \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem
   *   Date field.
   */
  public function getDateRange();

  /**
   * Set time slot date range.
   *
   * @param array $date_range_values
   *   Date range values.
   *
   * @see \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem
   */
  public function setDateRange(array $date_range_values);

  /**
   * Get current state.
   *
   * @return \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem
   *   State field.
   */
  public function getState();

  /**
   * Set time slot state.
   *
   * @param int $state
   *   See state constants.
   */
  public function setState($state);

}
