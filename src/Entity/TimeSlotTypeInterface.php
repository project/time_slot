<?php

namespace Drupal\time_slot\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Time slot type entities.
 */
interface TimeSlotTypeInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
