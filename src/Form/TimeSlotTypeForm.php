<?php

namespace Drupal\time_slot\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TimeSlotTypeForm.
 *
 * @package Drupal\time_slot\Form
 */
class TimeSlotTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $time_slot_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $time_slot_type->label(),
      '#description' => $this->t("Label for the Time slot type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $time_slot_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\time_slot\Entity\TimeSlotType::load',
      ],
      '#disabled' => !$time_slot_type->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $time_slot_type = $this->entity;
    $status = $time_slot_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Time slot type.', [
          '%label' => $time_slot_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Time slot type.', [
          '%label' => $time_slot_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($time_slot_type->urlInfo('collection'));
  }

}
