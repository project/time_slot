<?php

namespace Drupal\time_slot\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\time_slot\TimeSlotCalendarTrait;
use Drupal\time_slot\TimeSlotStorageInterface;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Class TimeSlotManagementForm.
 *
 * @package Drupal\time_slot\Form
 */
class TimeSlotManagementForm extends FormBase {

  use TimeSlotCalendarTrait;

  /**
   * Current year.
   *
   * @var int
   */
  protected $year;

  /**
   * Current week.
   *
   * @var int
   */
  protected $week;

  /**
   * Week begin midnight, probably Monday.
   *
   * @var \DateTime
   */
  protected $currentWeekBegin;

  /**
   * Week end midnight, probably Monday.
   *
   * @var \DateTime
   */
  protected $currentWeekEnd;

  /**
   * Content type.
   *
   * @var string
   */
  protected $contentType;

  /**
   * Content ID.
   *
   * @var int
   */
  protected $contentId;

  /**
   * Field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Field.
   *
   * @var \Drupal\time_slot\PLugin\Field\FieldType\TimeSlotManagement
   */
  protected $field;

  /**
   * Source entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * Current weekly pattern.
   *
   * @var array
   */
  protected $weeklyPattern;

  /**
   * Time slot storage.
   *
   * @var \Drupal\time_slot\TimeSlotStorageInterface
   */
  protected $timeSlotStorage;

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entity_type_manager, TimeSlotStorageInterface $time_slot_storage) {
    $this->entityTypeManager = $entity_type_manager;
    $this->timeSlotStorage = $time_slot_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('time_slot')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'time_slot_management_form';
  }

  /**
   * Management form.
   *
   * @param array $form
   *   Form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param string $content_type
   *   Content type.
   * @param string $content_id
   *   Content ID.
   * @param string $time_slot_management_field_name
   *   Field name.
   * @param int|string $year
   *   Year or 'now'.
   * @param int|string $week
   *   Week or 'now'.
   *
   * @return array
   *   Render array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $content_type = NULL, $content_id = NULL, $time_slot_management_field_name = NULL, $year = 'now', $week = 'now') {

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    try {
      $entity = $this->entityTypeManager->getStorage($content_type)->load($content_id);
    }
    catch (\Exception $e) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Could not load entity.'),
      ];
    }

    if (empty($entity)) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Entity not found.'),
      ];
    }

    try {
      $this->field = $entity->get($time_slot_management_field_name)->get(0);
    }
    catch (\Exception $e) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Field not found.'),
      ];
    }

    $this->contentType = $content_type;
    $this->contentId = (int) $content_id;
    $this->fieldName = $time_slot_management_field_name;

    if ($year == 'now') {
      $this->year = date('Y');
    }
    else {
      $this->year = (int) $year;
    }

    if ($week == 'now') {
      $this->week = date('W');
    }
    else {
      $this->week = (int) $week;
    }

    $this->currentWeekBegin = new \DateTime('midnight');
    $this->currentWeekBegin->setISODate($this->year, $this->week, 1);

    $this->currentWeekEnd = new \DateTime('midnight');
    $this->currentWeekEnd->setISODate($this->year, $this->week, 7);

    $field_value = $this->field->getValue();

    $pattern_slots = $this->weeklyContentByJson($field_value['weekly_pattern']);
    $pattern_table = $this->weeklyCalendar($pattern_slots);

    $render_array = [];

    $render_array['pattern'] = [
      '#type' => 'details',
      '#title' => $this->t('Show pattern'),
      'table' => $pattern_table,
    ];

    $render_array['time_slots'] = [
      '#type' => 'container',
      '#title' => $this->t('Time Slot Management'),
    ];

    $render_array['time_slots']['apply_pattern_button'] = [
      '#type' => 'button',
      '#value' => $this->t('Apply pattern'),
      '#ajax' => [
        'callback' => [$this, 'applyPattern'],
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $render_array['time_slots']['add_single_time_slot'] = [
      '#type' => 'link',
      '#title' => $this->t('Add single time slot'),
      '#url' => Url::fromRoute('entity.time_slot.add_form', [
        'time_slot_type' => 'default',
        'source_content_type' => $this->contentType,
        'source_content_id' => $this->contentId,
        'source_content_field' => $this->fieldName,
      ]),
      '#attributes' => [
        'class' => [
          'button',
        ],
      ],
    ];

    $render_array['time_slots']['navigation'] = [
      '#type' => 'container',
    ];

    $render_array['time_slots']['navigation']['last_week'] = [
      '#type' => 'link',
      '#title' => $this->t('Last week'),
      '#url' => Url::fromRoute('time_slot.management_form', [
        'time_slot_type' => 'default',
        'content_type' => $this->contentType,
        'content_id' => $this->contentId,
        'time_slot_management_field_name' => $this->fieldName,
        'year' => $this->year,
        'week' => $this->week - 1,
      ]),
    ];

    $render_array['time_slots']['navigation']['next_week'] = [
      '#type' => 'link',
      '#title' => $this->t('Next week'),
      '#url' => Url::fromRoute('time_slot.management_form', [
        'time_slot_type' => 'default',
        'content_type' => $this->contentType,
        'content_id' => $this->contentId,
        'time_slot_management_field_name' => $this->fieldName,
        'year' => $this->year,
        'week' => $this->week + 1,
      ]),
    ];

    $time_slots_ids = \Drupal::entityQuery('time_slot')
      ->condition('source_content_type', $this->contentType)
      ->condition('source_content_id', $this->contentId)
      ->condition('source_content_field', $this->fieldName)
      ->condition('date__value', $this->currentWeekBegin->format('Y-m-d'), '>=')
      ->condition('date__value', $this->currentWeekEnd->format('Y-m-d'), '<=')
      ->range(0, 50)
      ->execute();

    /** @var \Drupal\time_slot\Entity\TimeSlotInterface[] $time_slots */
    $time_slots = $this->timeSlotStorage->loadMultiple($time_slots_ids);
    $time_slot_elements = $this->weeklyContentByTimeSlots($time_slots);

    $render_array['time_slots']['table'] = $this->weeklyCalendar($time_slot_elements, $this->year, $this->week);
    $render_array['time_slots']['table']['#id'] = 'time-slot-management-table';

    return $render_array;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }
  }

  /**
   * Ajax response handler.
   *
   * @param array $form
   *   Current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return string
   *   Updated weekly pattern table HTML.
   */
  public function applyPattern(array $form, FormStateInterface $form_state) {
    $field_value = $this->field->getValue();

    $new_time_slots = [];
    foreach (\json_decode($field_value['weekly_pattern'], TRUE) as $entry) {
      $begin_date = strtotime('+' . ((int) $entry['day']) - 1 . ' weekdays +' . (int) $entry['offset'] . ' minutes', $this->currentWeekBegin->getTimestamp());
      $end_date = strtotime('+' . (int) $entry['length'] . ' minutes', $begin_date);
      $new_time_slot = $this->entityTypeManager->getStorage('time_slot')->create([
        'date' => [
          'value' => date(DATETIME_DATETIME_STORAGE_FORMAT, $begin_date),
          'end_value' => date(DATETIME_DATETIME_STORAGE_FORMAT, $end_date),
        ],
        'type' => $field_value['time_slot_type'],
        'source_content_type' => $this->contentType,
        'source_content_id' => $this->contentId,
        'source_content_field' => $this->fieldName,
        'state' => (int) $entry['status'],
      ]);
      $new_time_slot->save();
      $new_time_slots[] = $new_time_slot;
    }

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('[data-drupal-selector=time-slot-management-table', $this->weeklyCalendar($this->weeklyContentByTimeSlots($new_time_slots), $this->year, $this->week)));

    return $response;
  }

}
