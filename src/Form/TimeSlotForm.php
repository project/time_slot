<?php

namespace Drupal\time_slot\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Time slot edit forms.
 *
 * @ingroup time_slot
 */
class TimeSlotForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Time slot.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Time slot.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.time_slot.canonical', ['time_slot' => $entity->id()]);
  }

}
