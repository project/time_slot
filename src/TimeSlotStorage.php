<?php

namespace Drupal\time_slot;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\time_slot\Entity\TimeSlotInterface;

/**
 * Defines the storage handler class for Time slot entities.
 *
 * This extends the base storage class, adding required special handling for
 * Time slot entities.
 *
 * @ingroup time_slot
 */
class TimeSlotStorage extends SqlContentEntityStorage implements TimeSlotStorageInterface {

}
