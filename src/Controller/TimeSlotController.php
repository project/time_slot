<?php

namespace Drupal\time_slot\Controller;

use Drupal\time_slot\Entity\TimeSlotTypeInterface;
use Drupal\time_slot\TimeSlotStorageInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TimeSlotController.
 */
class TimeSlotController extends ControllerBase {

  /**
   * Time slot storage.
   *
   * @var \Drupal\time_slot\TimeSlotStorageInterface
   */
  protected $timeSlotStorage;

  /**
   * Time slot type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $timeSlotTypeStorage;

  /**
   * TimeSlotEntityAddController constructor.
   *
   * @param \Drupal\time_slot\TimeSlotStorageInterface $storage
   *   Time slot storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $type_storage
   *   Time slot type storage.
   */
  public function __construct(TimeSlotStorageInterface $storage, EntityStorageInterface $type_storage) {
    $this->timeSlotStorage = $storage;
    $this->timeSlotTypeStorage = $type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager->getStorage('time_slot'),
      $entity_type_manager->getStorage('time_slot_type')
    );
  }

  /**
   * Displays add links for available bundles/types for entity time_slot.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A render array for a list of the time_slot bundles/types that can be
   *   added or if there is only one type/bunlde defined for the site, the
   *   function returns the add page for that bundle/type.
   */
  public function add(Request $request) {
    $types = $this->timeSlotTypeStorage->loadMultiple();
    if ($types && count($types) == 1) {
      $type = reset($types);
      return $this->addForm($type, $request);
    }
    if (count($types) === 0) {
      return [
        '#markup' => $this->t('You have not created any %bundle types yet. @link to add a new type.', [
          '%bundle' => 'Time slot entity',
          '@link' => Link::createFromRoute($this->t('Go to the type creation page'), 'entity.time_slot_entity_type.add_form'),
        ]),
      ];
    }
    return ['#theme' => 'time_slot_entity_content_add_list', '#content' => $types];
  }

  /**
   * Presents the creation form for time_slot entities of given bundle/type.
   *
   * @param \Drupal\time_slot\Entity\TimeSlotTypeInterface $time_slot_type
   *   The custom bundle to add.
   * @param string $source_content_type
   *   Source content type.
   * @param int $source_content_id
   *   Source content ID.
   * @param string $source_content_field
   *   Source content field.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A form array as expected by drupal_render().
   */
  public function addForm(TimeSlotTypeInterface $time_slot_type, $source_content_type, $source_content_id, $source_content_field, Request $request) {
    $entity = $this->timeSlotStorage->create([
      'source_content_type' => $source_content_type,
      'source_content_id' => $source_content_id,
      'source_content_field' => $source_content_field,
      'type' => $time_slot_type->id(),
    ]);
    return $this->entityFormBuilder()->getForm($entity);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\time_slot\Entity\TimeSlotTypeInterface $time_slot_type
   *   The custom bundle/type being added.
   *
   * @return string
   *   The page title.
   */
  public function getAddFormTitle(TimeSlotTypeInterface $time_slot_type) {
    return t(
      'Create of bundle @label',
      [
        '@label' => $time_slot_type->label(),
      ]
    );
  }

}
