/**
 * @file
 * Time Slot Management.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Time slot management.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Time slot management.
   */
  Drupal.behaviors.timeSlotManagement = {
    attach: function (context, drupalSettings) {
      var container = jQuery('.time-slot-weekly-pattern-container', context);

      var weeklyPatternInput = container.find('.time-slot-hidden-weekly-pattern');

      var table = container.find('table.time-slot-weekly-table');
      table.find('.time-slot').click(function (e){
        e.preventDefault();
        var timeSlot = $(this);

        if (timeSlot.hasClass('time-slot-enabled')) {
          timeSlot.removeClass('time-slot-enabled');
          timeSlot.addClass('time-slot-disabled');
        }
        else {
          timeSlot.addClass('time-slot-enabled');
          timeSlot.removeClass('time-slot-disabled');
        }
        Drupal.behaviors.timeSlotManagement.insertWeeklyPattern(weeklyPatternInput, table);
      });
    },

    /**
     * Fill the weekly pattern input field with current table state.
     *
     * @param {jQuery} weeklyPatternInput - Weekly pattern
     * @param {jQuery} table - Calendar table
     */
    insertWeeklyPattern: function (weeklyPatternInput, table) {
      var weeklyPattern = [];
      table.find('.time-slot').each(function (index, timeSlot) {
        timeSlot = $(timeSlot);
        weeklyPattern.push({
          day: timeSlot.data('day'),
          offset: timeSlot.data('offset'),
          length: timeSlot.data('length'),
          // TODO: use TimeSlot entity constants.
          status: timeSlot.hasClass('time-slot-enabled') ? 1 : 0
        });
      });
      weeklyPatternInput.val(JSON.stringify(weeklyPattern));
    }
  };

})(jQuery, Drupal);
