/**
 * @file
 * Time Slot calendar views display.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Time slot calendar views display.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Time slot calendar views display.
   */
  Drupal.behaviors.timeSlotCalendarViewsDisplay = {
    attach: function (context, drupalSettings) {
      $('form.entity-browser-form', context).each(function (index, form) {
        form = $(form);

        form.find('.time-slot-weekly-table .views-field-entity-browser-select', context).each(function (index, inputContainer) {
          inputContainer = $(inputContainer);
          inputContainer.hide();

          /** @var jQuery timeSlot */
          var timeSlot = inputContainer.closest('.time-slot');
          timeSlot.click(function (event) {
            inputContainer.find('input').prop('checked', true);
            form.submit();
          });
        });
      });
    }
  };

})(jQuery, Drupal);
